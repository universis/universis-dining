// tslint:disable: trailing-comma
export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Sidebar.DiningRequests',
    key: 'Sidebar.DiningRequests',
    url: '/dining/requests',
    icon: 'fa fa-archive',
    index: 0
  },
  {
    name: 'Sidebar.DiningCards',
    key: 'Sidebar.DiningCards',
    url: '/dining/cards',
    icon: 'fa fa-id-card',
    index: 10
  },
  {
    name: 'Sidebar.Messages',
    key: 'Sidebar.Messages',
    url: '/dining/messages',
    icon: 'fa fa-envelope',
    index: 20
  },
  {
    name: 'Sidebar.DiningPreference',
    key: 'Sidebar.DiningPreference',
    url: '/dining/dining-preference',
    icon: 'fa fa-utensils',
    index: 20
  },
  {
    name: 'Sidebar.More',
    key: 'Sidebar.More',
    url: 'javascript:void(0)',
    class: 'open',
    icon: 'fa fa-cogs',
    index: 20,
    children: [
      {
        name: 'Sidebar.DiningAdmin',
        key: 'Sidebar.DiningAdmin',
        url: '/dining/admin-page',
        index: 5
      }
    ]
  },
];
