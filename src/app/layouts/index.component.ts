import {Component, OnInit, ViewChild, Input, AfterViewInit, AfterViewChecked} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {ConfigurationService, UserService, AppSidebarService, AuthGuard} from '@universis/common';


interface AppSettings {
  image?: string;
  title?: string;
}

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styles: [`
  :host {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-height: 100vh;
  }
  `]
})
export class IndexComponent implements AfterViewInit, AfterViewChecked {

  constructor(private _userService: UserService,
              private _configurationService: ConfigurationService,
              private _appSidebarService: AppSidebarService,
              private _translateService: TranslateService,
              private _authGuard: AuthGuard
            ) { }

  @ViewChild('appSidebarNav', {
    static: true
  }) appSidebarNav: any;
  @Input() user: any;
  public today = new Date();
  public languages: any;
  public currentLang!: string;
  public applicationImage: string | undefined;
  public applicationTitle!: string;

  ngAfterViewInit(): void {
    this._userService.getUser().then((user) => {
      // get sidebar navigation items
      const sidebarLocations = (this._appSidebarService as any).sidebarLocations.slice();
      sidebarLocations.forEach((sidebarLocation: { name: string; children?: Array<any> }) => {
        sidebarLocation.name = this._translateService.instant(sidebarLocation.name);
        if (Array.isArray(sidebarLocation.children)) {
          sidebarLocation.children.forEach((item) => {
            item.name = this._translateService.instant(item.name);
          });
        }});

      if (user != null) {
        try {
          // enumerate sidebar navigation items
          sidebarLocations.forEach((item) => {
            const canActivate = this._authGuard.canActivateLocation(item.url, user);
            if (canActivate == null) {
              if (item.children) {
                let canActivateOneChild = false;
                // enumerate children
                item.children.forEach((child) => {
                  // can activate child ?
                  const canActivateChild = this._authGuard.canActivateLocation(child.url, user);
                  if (canActivateChild && (canActivateChild.mask & 1) === 1) {
                    canActivateOneChild = true;
                  } else {
                    // hide child
                    child.class = 'd-none';
                  }
                });
                if (canActivateOneChild === false) {
                  // hide parent also
                  item.class = 'd-none';
                }
              } else {
                // hide item (no children)
                item.class = 'd-none';
              }
            } else if ((canActivate.mask & 1) === 0) {
              // hide item
              item.class = 'd-none';
            }
          });
          // and finally set sidebar items
          this.appSidebarNav.navItemsArray = sidebarLocations;
        } catch (err) {
          console.error('IndexComponent', 'An error occured while preparing application sidebar.');
          console.error('IndexComponent', err);
        }
      }
      // get current user
      this.user = user;
      // get current language
      this.currentLang = this._configurationService.currentLocale;
      // get languages
      this.languages = this._configurationService.settings.i18n?.locales;
      // get path of brand logo
      const appSettings:  AppSettings = this._configurationService.settings.app || {};
      this.applicationImage = appSettings && appSettings.image;
      // get application title
      this.applicationTitle = (appSettings && appSettings.title) || 'Universis';
    });
  }

  ngOnInit() {
    //
  }

  ngAfterViewChecked(): void {
    //
  }

  changeLanguage(lang: any): void {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }
}
