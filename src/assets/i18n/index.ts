import { el } from './el';
import { en } from './en';

export const DINING_ADMIN_LOCALES = {
  el,
  en
};
